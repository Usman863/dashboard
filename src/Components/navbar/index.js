import {Navbar,Nav,Form,FormControl,Button,NavDropdown} from 'react-bootstrap'
function Topbar() {
    return (
        <Navbar bg="light" expand="lg">
        {/* <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand> */}
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto" style={{display:'inline'}}>
          {/* <Form inline> */}
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            {/* <Button variant="outline-success">Search</Button> */}
          {/* </Form> */}
            <Nav.Link href="#home">Feedback</Nav.Link>
            <Nav.Link href="#link">Help</Nav.Link>
            
          </Nav>
          
        </Navbar.Collapse>
      </Navbar>
    );
  }
  
  export default Topbar;