import { BrowserRouter, Route, Switch } from 'react-router-dom';

function Home() {
    return (
        <div className="App">
            <h1>Oops! Page not found!</h1>
        </div>
    );
}

export default Home;
